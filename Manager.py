#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  3 2018

@author: Ani Hakobyan
"""
from Worker import Worker
from Product import Product
from UserBasket import UserBasket


class Administrator:
    # select sections such as worker, shop or accountancy
    @classmethod
    def selectSection(self):
        inData = input("WORKERS :1 \nSHOP :2\nACCOUNTANCY :3 \nEXIT :4\n: ")
        # if input data type is not int
        if not isTypeCorrect(inData, int):
            return Administrator.selectSection()

        inData = isTypeCorrect(inData, int)

        if inData == 1:
            return WorkerManager.chooseProc()
        if inData == 2:
            return ShopManager.chooseProc()
        if inData == 3:
            return Accountancy.totalProfit()
        if inData == 4:
            print("bye bye )))")
            return
        print("Enter 1, 2, 3 or 4")
        Administrator.selectSection()


class WorkerManager:
    # select sections such as add worker, edit salary or remove worker
    @classmethod
    def chooseProc(self):
        inData = input("ADD worker : 1 \nEDIT salary of worker: 2\nREMOVE worker : 3\nBACK : 4\n: ")
        # if input data type is not int
        if not isTypeCorrect(inData, int):
            return WorkerManager.chooseProc()

        inData = isTypeCorrect(inData, int)

        if inData == 1:
            return addWorker()
        if inData == 2:
            return editSalary()
        if inData == 3:
            return removeWorker()
        if inData == 4:
            return Administrator.selectSection()
        print("Enter 1, 2, 3 or 4")
        return WorkerManager.chooseProc()


class ShopManager:
    # select sections such as add product, edit quantity or remove product
    @classmethod
    def chooseProc(self):
        inData = input("ADD product : 1\nEDIT price of product : 2\nREMOVE : 3 \nBACK : 4\n: ")
        # if input data type is not int
        if not isTypeCorrect(inData, int):
            return ShopManager.chooseProc()

        inData = isTypeCorrect(inData, int)

        if inData == 1:
            return addProduct()
        if inData == 2:
            return editPrice()
        if inData == 3:
            return removeProduct()
        if inData == 4:
            return Administrator.selectSection()
        print("Enter 1, 2, 3 or 4")
        return ShopManager.chooseProc()


class Accountancy():
    # calculate total expence
    @classmethod
    def totalExpense(self):
        retVal = 0
        workers = Worker.getAllWorkers()
        for worker in workers:
            retVal += int(worker["salary"])
        return retVal

    # calculate total profit
    @classmethod
    def totalIncome(self):
        retVal = 0
        products = UserBasket.getAllProducts()
        for product in products:
            retVal += product['quantity'] * product['price']
        return retVal

    # calculate total profit
    @classmethod
    def totalProfit(self):
        print("profit is : {}".format(Accountancy.totalIncome() - Accountancy.totalExpense()))
        while True:
            inData = input("BACK : 1\n: ")
            if isTypeCorrect(inData, int):
                inData = isTypeCorrect(inData, int)
                break
        # inData = isTypeCorrect(inData, int)
        if inData == 1:
            return Administrator.selectSection()
        return Accountancy.totalProfit()


# Input the product name, price, quantity and add it
def addProduct():
    inData = input("BACK : 1\nCONTINUE : 2\n")

    if inData == "1":
        return ShopManager.chooseProc()
    if inData == "2":
        # Input product name, price and quantity
        name = input("product name\n: ")
        while True:
            price = input("price\n: ")
            if isTypeCorrect(price, int):
                price = isTypeCorrect(price, int)
                if price > 0:
                    break
                print("Enter a positive number")
        while True:
            quantity = input("quantity\n: ")
            if isTypeCorrect(quantity, int):
                quantity = isTypeCorrect(quantity, int)
                if quantity > 0:
                    break
                print("Enter a positive number")
                # Input id of the worker
        while True:
            id = input("ID \n: ")
            if isTypeCorrect(id, int):
                id = isTypeCorrect(id, int)
                if id > 0:
                    break
                print("Enter a positive number")
        # add product
        s = Product(id, name, price, quantity)
        Product.addProduct(s)
        return addProduct()

    print("Enter 1 or 2")
    return addProduct()


# change price of the product
def editPrice():
    inData = input("BACK : 1\nCONTINUE : 2\n")

    if inData == "1":
        return ShopManager.chooseProc()
    if inData == "2":
        print("id  name price quantity")
        printShopProducts()
        # input product id
        while True:
            id = input("Enter product ID\n : ")
            if isTypeCorrect(id, int):
                break
        id = isTypeCorrect(id, int)

        # input new price
        while True:
            price = input("Enter new price \n : ")
            if isTypeCorrect(price, int):
                price = isTypeCorrect(price, int)
                if price > 0:
                    break
                print("Enter a positive number")
        Product.editPrice(id, price)
        return editPrice()
    print("Enter 1 or 2")
    return editPrice()


# remove product with id
def removeProduct():
    inData = input("BACK : 1\nCONTINUE : 2\n")

    if inData == "1":
        return ShopManager.chooseProc()
    if inData == "2":
        print("id  name price quantity")
        printShopProducts()
        # input product id
        while True:
            id = input("Enter product id and remove product\n : ")
            if isTypeCorrect(id, int):
                id = isTypeCorrect(id, int)
                break

        # confirm the remove operation and remove product
        inData = input("Do you want to remove that product?\n YES: 1\n No: enter any int number\n: ")
        if inData == "1":
            Product.removeProduct(id)
        return removeProduct()
    print("Enter 1 or 2")
    return removeProduct()


def printShopProducts():
    products = Product.getAllProducts()
    for product in products:
        print("{} {} {} {}".format(product["id"], product["name"], product["price"], product["quantity"]))


# Input the worker info and add       
def addWorker():
    inData = input("BACK : 1\nCONTINUE : 2\n")

    if inData == "1":
        return WorkerManager.chooseProc()
    if inData == "2":

        # Input name and surname of the worker
        inData = input("Enter the worker data('name, surname') \n: ")
        # Separate the name and surname
        inData = inData.split(" ")
        if len(inData) != 2:
            print("The name or surname of the worker are absence!")
            return addWorker()
        fname = inData[0]
        lname = inData[1]

        # Input salary of the worker
        while True:
            salary = input("Enter salary \n: ")
            if isTypeCorrect(salary, int):
                salary = isTypeCorrect(salary, int)
                if salary > 0:
                    break
                print("Enter a positive number")

        # Input id of the worker
        while True:
            id = input("ID \n: ")
            if isTypeCorrect(id, int):
                id = isTypeCorrect(id, int)
                if id > 0:
                    break
                print("Enter a positive number")

        w = Worker(id, fname, lname, salary)
        Worker.addWorker(w)
        return addWorker()

    print("Enter 1 or 2")
    return addWorker()


# change salary of the worker
def editSalary():
    inData = input("BACK : 1\nCONTINUE : 2\n")
    if inData == "1":
        return WorkerManager.chooseProc()
    if inData == "2":

        print("id name salary")
        printWorkers()
        # input worker id
        while True:
            id = input("Enter worker ID\n : ")
            if isTypeCorrect(id, int):
                break
        id = isTypeCorrect(id, int)

        # input new salary
        while True:
            salary = input("Enter worker salary\n : ")
            if isTypeCorrect(salary, int):
                salary = isTypeCorrect(salary, int)
                if salary > 0:
                    break
                    print("Enter a positive number")

        Worker.editWorkerSalary(id, salary)
        return editSalary()
    print("enter 1 or 2")
    return editSalary()


# remove worker with id
def removeWorker():
    inData = input("BACK : 1\nCONTINUE : 2\n")

    if inData == "1":
        return WorkerManager.chooseProc()
    if inData == "2":
        print("id name salary")
        printWorkers()
        # input worker id
        while True:
            id = input("Enter worker ID\n : ")
            if isTypeCorrect(id, int):
                break
        id = isTypeCorrect(id, int)

        # confirm the remove operation and remove worker

        inData = input("Do you want to remove that worker?\n YES: 1\n No: enter anything\n: ")
        if inData == "1":
            Worker.removeWorker(id)
        return removeWorker()
    print("enter 1 or 2")
    return removeWorker()


def printWorkers():
    workers = Worker.getAllWorkers()
    for worker in workers:
        print("{} {} {}".format(worker["id"], worker["fname"], worker["salary"]))


def isTypeCorrect(inData, _type):
    try:
        return _type(inData)
    except ValueError:
        print("Data type is incorrect!")
        return False
