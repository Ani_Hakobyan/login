#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  3 2018

@author: Ani Hakobyan
"""

class Person(object):
    def __init__(self, id, fname, lname):
        self.id = id
        self.fname = fname
        self.lname = lname
        
    def name(self):
        return "{} {} {}".format(self.id, self.fname,self.lname)
    
    

