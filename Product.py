#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  3  2018

@author: Ani Hakobyan
"""
import requests


class Product():
    def __init__(self, id, name, price, quantity):
        self.id = id
        self.name = name
        self.quantity = quantity
        self.price = price

    @classmethod
    def getNameAndPrice(self, id):
        r = requests.get('http://127.0.0.1:5000/product/{}'.format(id))
        return r.json()

    def addProduct(self):
        r = requests.post(
            'http://127.0.0.1:5000/product/{}?name={}&price={}&quantity={}'.format(self.id, self.name, self.price,
                                                                                   self.quantity))
        print(r.json())

    @classmethod
    def getAllProducts(self):
        r = requests.get('http://127.0.0.1:5000/product/0')
        return r.json()

    @classmethod
    def editPrice(self, id, price):
        r = requests.put('http://127.0.0.1:5000/product/{}?price={}'.format(id, price))
        print(r.json())

    @classmethod
    def removeProduct(self, id):
        r = requests.delete('http://127.0.0.1:5000/product/{}'.format(id))
        print(r.json())
