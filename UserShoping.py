#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4  2018

@author: Ani Hakobyan
"""
from UserBasket import UserBasket
from Product import Product
from Manager import isTypeCorrect
from Manager import printShopProducts

class UserShoping:
    @classmethod       
    def chooseProc(self):
        inData = input("BUY : 1 \nCHANGE  quantity of bought product : 2 \nREMOVE bought product: 3\nEXIT : 4\n: ")
        if inData == "1":
            return buyProduct()
        if inData == "2":
            return changeQuantity()
        if inData == "3":
            return remove()
        if inData == "4":
            print ("bye bye )))" )
            return
        print ("Enter 1, 2, 3 or 4")
        return UserShoping.chooseProc()
    
# input product id and qauntity for buy it          
def buyProduct():
    inData = input("BACK : 1\nCONTINUE : 2\n")

    if inData == "1":
        return UserShoping.chooseProc()
    if inData == "2":
        print ("id  product  price   qauntity" )
        printShopProducts()
        #input product id
        while True:
            id = input("Enter id_ product\n : ")
            if isTypeCorrect(id,int):
                break
        id = isTypeCorrect(id,int)

        #input quantity
        while True:
            quantity = input("Enter quantity\n : ")
            if isTypeCorrect(quantity,int):
                quantity = isTypeCorrect(quantity,int)
                if quantity > 0:
                    break
                print ("Enter a positive number")
        #add product into user basket
        res = Product.getNameAndPrice(id).split(" ")

        name = res[0]
        price = res[1]

        p = UserBasket(id,name,quantity,price)
        UserBasket.addProduct(p)
        return buyProduct()
    print ("Enter 1 or 2")
    return buyProduct()

# input id and new quantity for change product quantity                    
def changeQuantity():
    inData = input("BACK : 1\nCONTINUE : 2\n")

    if inData == "1":
        return UserShoping.chooseProc()
    if inData == "2":
        print ("id  product  qauntity" )
        printBasketProducts()
        # input product id
        while True:
            id = input("Enter product ID and change quantity\n : ")
            if isTypeCorrect(id,int):
                break
        id = isTypeCorrect(id,int)

        #input new quantity
        while True:
            quantity = input("Enter product quantity\n : ")
            if isTypeCorrect(quantity,int):
                quantity = isTypeCorrect(quantity,int)
                if quantity > 0:
                    break
                print ("Enter a positive number")
        UserBasket.editProductQuantity(id,quantity)
        return changeQuantity()
    print ("Enter 1 or 2")
    return changeQuantity()

#remove product from user basket    
def remove():
    inData = input("BACK : 1\nCONTINUE : 2\n")

    if inData == "1":
        return UserShoping.chooseProc()
    if inData == "2":
       print ("id  product  qauntity" )
       printBasketProducts() 
       #input id
       while True:
           id = input("Enter product id and remove product from basket\n : ")
           if isTypeCorrect(id,int):
               break
       id = isTypeCorrect(id,int)

       #confirm the remove operation and remove product
       inData = input("Do you want to remove that product?\n YES: 1\n No: enter anything\n: ")
       if inData == "1":
          UserBasket.removeProduct(id)
       return remove()
    print ("Enter 1 or 2")
    return changeQuantity()

def printBasketProducts():
    products = UserBasket.getAllProducts()
    for product in products:
            print ("{} {} {}".format(product["id"],product["name"],product["quantity"]))


