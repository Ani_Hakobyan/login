#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4 2018

@author: Ani Hakobyan
"""

from Product import Product
import requests

class UserBasket(Product):
    def __init__(self,id,name,quantity,price):
        Product.id = id
        Product.name = name
        Product.quantity = quantity
        Product.price = price
    
    #add product into user basket and decrement it's quantity into shop
    def addProduct(self):
        print (self.name)
        r = requests.post(
            'http://127.0.0.1:5000/basketProduct/{}?name={}&price={}&quantity={}'.format(self.id, self.name, self.price,
                                                                                   self.quantity))
        print(r.json())


    @classmethod
    def getAllProducts(self):
        r = requests.get('http://127.0.0.1:5000/basketProduct/1')
        return r.json()
 
    @classmethod
    def editProductQuantity(self,id,quantity):
        r = requests.put('http://127.0.0.1:5000/basketProduct/{}?quantity={}'.format(id, quantity))
        print(r.json())

    # add product into shop and remove it into shop
    @classmethod
    def removeProduct(self,id):
        r = requests.delete('http://127.0.0.1:5000/basketProduct/{}'.format(id))
        print(r.json())

    


