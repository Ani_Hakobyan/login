#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  3  2018

@author: Ani Hakobyan
"""
import requests
from Person import Person


class Worker(Person):
    def __init__(self, id, fname, lname, salary):
        self.salary = salary
        Person.id = id
        Person.fname = fname
        Person.lname = lname

    def addWorker(self):
        r = requests.post(
            'http://127.0.0.1:5000/worker/{}?fname={}&lname={}&salary={}'.format(self.id, self.fname, self.lname,
                                                                                 self.salary))
        print(r.json())

    @classmethod
    def getAllWorkers(self):
        r = requests.get('http://127.0.0.1:5000/worker/1')
        return r.json()

    @classmethod
    def editWorkerSalary(self, id, salary):
        r = requests.put('http://127.0.0.1:5000/worker/{}?salary={}'.format(id, salary))
        print(r.json())

    @classmethod
    def removeWorker(self, id):
        r = requests.delete('http://127.0.0.1:5000/worker/{}'.format(id))
        print(r.json())
