#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4  2018

@author: Ani Hakobyan
"""
from Admin import Admin
from Manager import Administrator
from Manager import isTypeCorrect
from UserShoping import UserShoping
import requests


class Main:
    @classmethod
    def login(self):
        inData = input("BACK : 1\nCONTINUE : 2\n")

        if inData == "1":
            return Main.main()
        if inData == "2":
            # username = ani
            # password = 1234

            # input username
            name = input("Enter username : ")
            # input password
            password = input("Enter password : ")

            # check is admin
            if (Admin.checkAdmin(name, password)):
                return Administrator.selectSection()
            return Main.login()

        print("enter 1 or 2")
        return Main.login()

    @classmethod
    def main(self):
        inData = input("SHOP :1 \nADMIN :2 \n: ")

        if (inData == "1"):
            return UserShoping.chooseProc()
        if (inData == "2"):
            return Main.login()
        print("Enter 1 or 2")
        return Main.main()


print("Hy..")
Main.main()
