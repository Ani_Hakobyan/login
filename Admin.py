#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  3 2018

@author: Ani Hakobyan
"""
import requests


class Admin():
    def __init__(self, id, username, password):
        self.id = id
        self.username = username
        self.password = password

    @classmethod
    def checkAdmin(self, username, password):
        # checks admin on the server
        r = requests.post('http://127.0.0.1:5000/admin/{}/{}'.format(username, password))
        print(r.json())
        if r.ok:
            return True
        return False
